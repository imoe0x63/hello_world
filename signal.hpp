
#ifndef SIGNAL_HPP
#define SIGNAL_HPP

template<typename R, typename ... A> struct signal{};
template<typename R, typename ... A> struct signal<R(A...)> {
    private:
        typedef R (*func_t)(A...);
    
        func_t m_func;
    public:
        signal(func_t __f) : m_func(__f) {}

        R operator()(const A& ... arg) { return m_func(arg...); }
};


#endif//SIGNAL_HPP