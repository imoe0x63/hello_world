#include "signal.hpp"
#include "vec.hpp"

#include <cstdio>

void greet(void) {
    fputs(" Hello, everybody !!", stdout);
}


int main(int argc, char** argv) {
    // test signal
    signal<void(void)> greet_f(greet);
    greet_f();
    
    
    //test vec
    vec<char> str; 
    fputs(str.data(), stdout);
    
    return 0;
}