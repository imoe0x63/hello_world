
#ifndef STACK_HPP
#define STACK_HPP

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <initializer_list>

#define MAX_STACK_CAP        (2<<22)
#define MIN_STACK_CAP        (32)


template<typename Type> struct stack {
    private:
        Type*         _data;
        size_t        _size;
        size_t        _cap;
    
    
        enum class sz_flag : bool { TOO_BIG, TOO_SMALL };
        size_t sz_err( const sz_flag& flg) {
            return flg == sz_flag::TOO_BIG 
                ? (fputs("\n[ERROR] : the requested size to be allocated is more than MAX_STACK_CAP we have allocated MAX_STACK_CAP", stdout), MAX_STACK_CAP) 
                : (fputs("\n[ERROR] : the requested size to be allocated is less than MIN_STACK_CAP we have allocated MIN_STACK_CAP", stdout), MIN_STACK_CAP);
        }

    public:
        stack() : _cap(MIN_STACK_CAP), _size(0) {
            _data = (Type*)malloc( sizeof(Type) * _cap);
        }
        
        stack( std::initializer_list<Type> __ll) : 
            _cap(
                __ll.size() > MAX_STACK_CAP
                ? sz_err(sz_flag::TOO_BIG)
                : __ll.size()
            ),
            _size( _cap )
        {
            _data = (Type*)malloc( sizeof(Type) * _cap);
            size_t ctr=0;
            for(const auto& im : __ll) _data[ctr++] = im;
        }
        
        void push(const Type& __im) {
            if(_size < _cap) {
                _data[_size++] = __im;
            } else {
                if(_cap < MAX_STACK_CAP){ 
                    if(MAX_STACK_CAP - _cap > MIN_STACK_CAP) {
                        _cap += MIN_STACK_CAP;
                    } else {
                        _cap = MAX_STACK_CAP;
                    }
                    
                    // create temporary stack memory
                    Type* tmp = (Type*)malloc( sizeof(Type) * _size);
                    memcpy(tmp, _data, _size);
                    
                    // increase stack memory size and re-evaluate data
                    _data = (Type*)realloc( _data, _cap);
                    for(size_t ctr=0; ctr< _size; ctr++) _data[ctr] = tmp[ctr];
                    
                    // free temporary memory
                    free(tmp);
                    
                    _data[_size++] = __im;
                } else {
                    //sz_err(sz_flag::TOO_BIG);
                    fputs("\n [ERROR] THE STACK IS FULL can't add more data to it ", stdout);
                }
            }
        }
        
        void pop() {_size--;}
        
        constexpr size_t size() const noexcept { return _size;}
        constexpr size_t capacity() const noexcept { return _cap;}

        Type* begin() { return _data; }
        Type* end(){ return _data + _size; }

        
        Type* data() const noexcept { return _data; }
        Type& operator[](const size_t& idx) {
            return _data[idx < _size ? idx : (fputs("\n [ERRPR] : index request is not in the stack", stdout),0)];
        }   



        ~stack() {
            free(_data);
        }
};

#endif//STACK_HPP